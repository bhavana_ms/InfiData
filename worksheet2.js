var color=['red','blue','green','orange','black'];
alert(color[3]);

alert(color.length);
console.log('before '+color);
color.push('purple'); //push at end
console.log('after '+color);
color.pop(); //pop at end
console.log('after pop '+color);
color.shift();  // remove at beginning
console.log('after shift '+color);
color.unshift('yellow'); //add at beginning
console.log('after unshift '+color);


/*   ctrl+shift+/ to comment use java comment syntax to comments*/

$('#b').click(function(){
	alert("hello");
}); 

$('#b').dblclick(function(){
	alert("u clicked twice");
});

for (var i = color.length - 1; i >= 0; i--) {
	$('#color').append("<li>"+color[i]+"</li>"); 
	//get the element and append the colors one by one using for loop
}

//objects - JSON javscript object notation

var car={
	id : 13,
	brand : "Chevrolet",
	model : "Chevy 1967 Impala",
	clr : ['red','blue','black'],
	isReleased : true,
	helpline : {
		Kansas : "666666",
		India : "8654953215",
		Germany : "9635215463"
	}
	//img : "https://www.prettymotors.com/uploads/2014/12/1967-Chevrolet-Impala-4-door.jpg"
};

alert(car.model);
alert(car.helpline.Kansas);
alert(car.clr[2]);

//var str=`<img href="https://www.prettymotors.com/uploads/2014/12/1967-Chevrolet-Impala-4-door.jpg">`+

//$('#imp').append(str);


